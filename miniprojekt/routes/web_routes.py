from flask import Flask, jsonify, render_template, Blueprint, request, redirect
import data as DB

web_routes_bp = Blueprint('web_routes', __name__)

user="Login" #name of logged in User used for the name of the Loggin/User page link
DbCheck=False #true if user is in DB
PassCheck=True #true if the passwords match
RegCheck=False #true if user tries to login but isnt registered (special condition)

@web_routes_bp.route("/")
def start():
    return redirect("/login")

@web_routes_bp.route("/user")
def home():
        global user
        last=DB.get_last_item()
        return render_template("index.html",user=user,loggedin=(user != "Login"),TempSt=round(last[2],2),TimeSt=last[1])

@web_routes_bp.route("/login",methods = {"GET","POST"})
def login():
    global DbCheck,PassCheck,RegCheck,user

    if request.method == "GET":
        return render_template("login.html",user=user,checked=DbCheck,passcheck=PassCheck,loggedin=False)
    else:
        usr = request.args.get('usr')
        passw=DB.hash_password(request.args.get('pass'))
        DbCheck= DB.check_entry(usr)
        if DbCheck:
                PassCheck=DB.check_pass(usr,passw) 
                print(PassCheck)
                if PassCheck:
                        user=usr
                        return redirect("/user")
                else:
                        return redirect("/login")
        else:
                RegCheck= False
                return redirect("/register")

@web_routes_bp.route("/register",methods = {"GET","POST"})
def register():
    global DbCheck,RegCheck
    if request.method == "GET":
        return render_template("register.html",cheched=RegCheck,loggedin=False)
    else:
        RegCheck=True
        usr = request.args.get('usr')
        passw=DB.hash_password(request.args.get('pass'))
        DbCheck=DB.check_entry(usr)
        if not DbCheck:
              DB.add_entry(usr,passw)

        return redirect("/login")
              

@web_routes_bp.route("/logout")
def logout():
        global user,DbCheck,RegCheck
        user="Login"
        DbCheck=False
        RegCheck=False
        return redirect("/")


@web_routes_bp.route('/Meas/<int:id>')
def getItem(id):
    global user
    return render_template("measView.html",id=id,TempSt=round(DB.get_data_ID(id)[0][2],2),TimeSt=DB.get_data_ID(id)[0][1],user=user,loggedin=True)



@web_routes_bp.route('/get_condition')
def get_condition():
    global DbCheck
    return jsonify({'condition': DbCheck})

from flask import jsonify, request, Blueprint
import data as DB
import json as j

api_routes_bp = Blueprint('api_routes', __name__)



@api_routes_bp.route('/api/get_data', methods=['POST'])
def get_data():
    number_input = int(request.json.get('numberInput'))
    return jsonify(DB.get_data(number_input))

@api_routes_bp.route('/api/del_data', methods=['POST'])
def delete_data():
    number_input = int(request.json.get('numberInput'))
    return jsonify(DB.del_data(number_input))
    
@api_routes_bp.route('/api/del_data2', methods=['POST'])
def delete_data2():
    number_input = int(request.json.get('numberInput'))
    return jsonify(DB.del_data2(number_input))

@api_routes_bp.route('/api/get_last')
def getlast():
    return jsonify(DB.get_last_item())

@api_routes_bp.route('/api/add', methods=['POST'])
def entryAdd():
    data=request.get_json()
    print(data)
    return jsonify(DB.add_Item(data))

from machine import Pin, Timer, SPI
import sys, random, utime as t
import typek



Drdy = Pin(13,mode=Pin.IN)
Start = Pin(12,mode=Pin.OUT,)
Reset = Pin(11,mode=Pin.OUT)
Reset.on()

ads=SPI(0,baudrate=4000000,sck=Pin(18),miso=Pin(16),mosi=Pin(19))
NBITS=24
VREF=2048#2560
PGA=32
LSB=(2*VREF)/(PGA*(2**(NBITS-1)))
REFLSB=(2*VREF)/(1*(2**(NBITS-1)))
#print(LSB)
TAMB=25 #°C
AMB=118 #118mv coresponds to 25 °C
AMBCOEF=0.405 #mV/°C
SELFOCAL=b'\x62'
RESET=b'\x06'
SDATAC=b'\x16'
NOP=b'\xFF'
RDATA=b'\x12'
SYNC=b'\x04\x04'

def HexToDec(input):
    n = len(input)    
    # Convert bytes to an integer
    dec = 0
    for byte in input:
        dec = (dec << 8) | byte
    
    # Calculate the 2's complement value
    #if input[0] & 0x80:  # if the sign bit is set
        #dec -= 1 << (n * 8)
    if dec <= 0x7FFFFF and dec >= 0x400000:
        dec = -( (~dec) & 0xFFFFF)
    return dec

def Setup():
    t.sleep_ms(30)
    Start.on()
    ads.write(RESET)
    t.sleep_ms(1)
    ads.write(SDATAC)
    ads.write(b'\x40\x03\x00\x01\x30\x56')#01010110
    ads.write(SELFOCAL)
    t.sleep_ms(200)
    ads.write(SYNC)

def tempGet():
    if Drdy.value() != 1:
        ktc=TC_read()
        amb=AMB_read()
        #print(str("ambient temp: {:.2f} degC").format(amb))
        #print(str("TC temp: {:.2f} degC").format(typek.get_temp(int(amb),ktc)))
        return(str("{:.2f}").format(typek.get_temp(int(amb),ktc)))
    return("NONE")

def TC_read():
    ads.write(b'\x40\x02\x01\x00\x30')
    t.sleep_ms(20)
    ads.write(RDATA)
    KtmpBytes=ads.read(3,int("FF",16))
    #print("Dec= {}",KtmpBytes)
    ktc=HexToDec(KtmpBytes)
    #print("katc=",ktc)
    ktc=ktc*LSB
    return ktc

def AMB_read():
    ads.write(b'\x42\x00\x33')
    t.sleep_ms(20)
    ads.write(RDATA)
    tmpBytes=ads.read(3,int('FF',16))
    ambient=HexToDec(tmpBytes)
    ambient=ambient*REFLSB
    ambient=(ambient-AMB)/AMBCOEF
    ambient+=TAMB
    return ambient

Setup()
led = Pin('LED', Pin.OUT)
min = 20
max = 10
while True:
    temp = tempGet()
    if temp != "NONE":
        if float(temp) < min:
            min = float(temp)
        if float(temp) > max:
            max = float(temp)
        req = sys.stdin.readline().strip()
        if req == "send":
            led.toggle()
            t.sleep_ms(1)
            led.toggle()
            print("{}".format(temp))
    #print(str("min: {:.2F} degC, max: {:.2f} degC").format(min,max))
    t.sleep_ms(1000)
    

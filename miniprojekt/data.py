import sqlite3, os,random,hashlib,time
from datetime import datetime, timedelta

dbFile = os.path.join(os.getcwd(), 'NSImini.db')

def generate_db():
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    
    createQuery = f"""
    CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user TEXT UNIQUE NOT NULL,
        pass TEXT NOT NULL
    );
    """
    cursor.execute(createQuery) 
    createQuery = f"""
    CREATE TABLE IF NOT EXISTS data (
        id INTEGER PRIMARY KEY,
        time TEXT NOT NULL,
        temp REAL NOT NULL
    );
    """
    cursor.execute(createQuery)
    conn.close()

def generate_data():
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()

    createQuery = f"""
    CREATE TABLE IF NOT EXISTS data (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        time TEXT UNIQUE NOT NULL,
        temp TEXT NOT NULL
    );
    """
    cursor.execute(createQuery)
    conn.close()
    
    numRecords = 2349
    tableName = 'data'
    minTemp = 10.0
    maxTemp = 80.0
    startDate = '2024-03-21 00:00:00'
    period = 300
    
    '''
    countQuery = f"SELECT COUNT(*) FROM {tableName}"
    cursor.execute(countQuery)
    count = cursor.fetchone()[0]
    if count > 0:
        conn.close()
    else:    
        if count == 0:
            startTime = datetime.strptime(startDate, '%Y-%m-%d %H:%M:%S')
        else:
            maxTimestampQuery = f"SELECT MAX(time) FROM {tableName}"
            cursor.execute(maxTimestampQuery)
            maxTimestamp = cursor.fetchone()[0]
            maxTimestampDatetime = datetime.strptime(maxTimestamp, '%Y-%m-%d %H:%M:%S')
            startTime = maxTimestampDatetime + timedelta(seconds=period)
        
        for i in range(numRecords):
            timestamp = startTime + timedelta(seconds=i*period)
            temperature = random.uniform(minTemp, maxTemp)
            insertQuery = f"INSERT INTO {tableName} (time, temp) VALUES (?, ?)"
            cursor.execute(insertQuery, (timestamp.strftime('%Y-%m-%d %H:%M:%S'), temperature))
        conn.commit()
        conn.close()
'''
def get_data(number_input):
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data LIMIT ?", (number_input,))
    data = cursor.fetchall()
    conn.close()
    return data


def add_Item(data):
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    insertQuery = f"INSERT INTO data (time, temp) VALUES (?, ?)"
    cursor.execute(insertQuery, (data['time'], data['temp']))
    conn.commit()
    conn.close()
    return "Done"

def del_data(number_input):
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    cursor.execute("DELETE FROM data WHERE id = ?", (number_input,))
    conn.commit()
    conn.close()
    return "Done"


def del_data2(number_input):
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    cursor.execute("SELECT id FROM data LIMIT ?", (number_input,))
    ids_to_delete = [row[0] for row in cursor.fetchall()]
    for entry_id in ids_to_delete:
        cursor.execute("DELETE FROM data WHERE id = ?", (entry_id,))
    conn.commit()
    conn.close()
    return "Done"

def get_last_item():
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data ORDER BY id DESC LIMIT 1")
    last_item = cursor.fetchone()
    conn.close()
    return last_item

def get_data_ID(id):
    global dbFile
    conn = sqlite3.connect(dbFile)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM data WHERE id = ?", (id,))
    data = cursor.fetchall()
    conn.close()
    return data
     

"""User Stuff"""
def hash_password(password):
    # Hash the password using SHA-256 algorithm
    return hashlib.sha256(password.encode()).hexdigest()

def check_entry(username):
        global dbFile
        # Connect to the SQLite database
        conn = sqlite3.connect(dbFile)  # Change 'your_database.db' to your actual database file name
        cursor = conn.cursor()
        # Execute a SELECT query to check if the username already exists in the database
        cursor.execute("SELECT * FROM users WHERE user = ?", (username,))
        existing_entry = cursor.fetchone()  # Fetch one row from the result set

        conn.close()  # Close the database connection

        # Check if an entry with the given username exists
        if existing_entry:
                return True  # Entry exists
        else:
                return False  # Entry does not exist
        
def check_pass(username,password):
        global dbFile   
        # Connect to the SQLite database
        conn = sqlite3.connect(dbFile)  # Change 'your_database.db' to your actual database file name
        cursor = conn.cursor()

        # Execute a SELECT query to check if the username already exists in the database
        cursor.execute("SELECT pass FROM users WHERE user = ?", (username,))
        existing_entry = cursor.fetchone()  # Fetch one row from the result set

        conn.close()  # Close the database connection

        # Check if an entry with the given username exists
        if existing_entry[0]==password:
                return True  # Entry exists
        else:
                return False  # Entry does not exist
        
def add_entry(username,password):
        global dbFile
     # Connect to the SQLite database
        conn = sqlite3.connect(dbFile)
        cursor = conn.cursor()

        insertQuery = f"INSERT INTO users (user, pass) VALUES (?, ?)"
        cursor.execute(insertQuery, (username, password))

        conn.commit()
        conn.close()  # Close the database connection


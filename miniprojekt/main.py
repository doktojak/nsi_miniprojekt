from flask import Flask
import datetime,serial,time
import threading
import data as DB
from routes import register_routes

app = Flask(__name__)
DB.generate_db()
DB.generate_data()

register_routes(app)

try:
    ser = serial.Serial('COM6', 9600)
    ser.flushInput()
except:
    print("Unable to open COM port")

def read_serial():
        global ser
        while True:
            ser.write('send\r'.encode())
            ser.flushOutput()
            time.sleep(0.1)
            if ser and ser.in_waiting > 0:
                try:
                    MeasTemp = ser.readline().decode('utf-8').rstrip()
                    #ser.flushInput()
                    # Attempt to convert the line to a float, assuming the format is xx.xx
                    MeasTemp = float(MeasTemp)
                except ValueError:
                    # Handle the case where the line cannot be converted to a float
                    print(f"Received invalid data: {MeasTemp}")
                timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                DB.add_Item({'time': timestamp, 'temp': MeasTemp})
            time.sleep(2)
   
serial_thread = threading.Thread(target=read_serial)
serial_thread.daemon = True
serial_thread.start()
if __name__ == '__main__':
    threading.Thread(target=read_serial).start()
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)
